<?php

use App\Controllers\BagUserController;
use App\Controllers\MagasinController;
use App\Controllers\SearchProductController;
use App\Controllers\UsersController;
use App\middlewares\Authentification;

require_once './vendor/autoload.php';

session_start();

switch (getUri()) {
    /**
     * chemin page
     */
    case '/':
        $auth = new Authentification();
        $auth->auth();
        $magasin = new MagasinController();
        echo $magasin->index();
        break;

    case '/connexion':
        $auth = new Authentification();
        $auth->auth();
        $magasin = new MagasinController();
        echo $magasin->connexion();
        break;

    case '/home':
        $auth = new Authentification();
        $auth->noAuth();
        $magasin = new MagasinController();
        echo $magasin->home();
        break;

    case '/produit':
        $auth = new Authentification();
        $auth->noAuth();
        $magasin = new MagasinController();
        echo $magasin->produit();
        break;
    case '/bag' :
        $auth = new Authentification();
        $auth->noAuth();
        $magasin = new MagasinController();
        echo $magasin->bagUser();
        break;
    case '/paiement' :
        $auth = new Authentification();
        $auth->noAuth();
        $magasin = new MagasinController();
        echo $magasin->paiementUser();
        break;
    /**
     * chemin formulaire
     */
    case '/form-inscription':
        $form = new UsersController();
        $form->CreateUser();
        break;

    case '/form-connect':
        $form = new UsersController();
        $form->Connect();
        break;

    case '/form-addbag':
        $form = new BagUserController();
        $form->addBagUser();
        break;

    case '/deleteArticleBag':
        $form = new BagUserController();
        $form->deleteArticleBag();
        break;
    case '/searchProduct':
        $form = new SearchProductController();
        $form->search();
        break;
    case '/disconnect':
        $form = new UsersController();
        $form->Disconnect();
        break;

}
