<?php

namespace App\Validators;

use Respect\Validation\Exceptions\ValidationException;

class UsersValidator
{
    public function UserValidate($email, $password): bool
    {
        $emailValidator = \Respect\Validation\validator::email()->notBlank()->length(3,320);
        $passwordValidator = \Respect\Validation\Validator::stringVal()->notBlank()->length(3,255);

        try {
            $emailValidator->check($email);
            $passwordValidator->check($password);
        } catch(ValidationException $exception) {
//            echo $exception->getMessage();
            header('Location: /connexion');
            exit;
        }
        return true;
    }
}