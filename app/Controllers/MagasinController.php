<?php

namespace App\Controllers;

use App\Models\ProductModel;

class MagasinController extends Controller
{

    public function index(): string
    {
        return $this->template->render('users/inscription.twig',);
    }

    public function connexion() : string
    {
        return $this->template->render('users/connexion.twig',);
    }

    public function home(): string
    {
        $product = new ProductModel();
        $page=$_GET['page']??1;

        $i = 1 ;
        $numberPerPage = 8;
        $totalArticle = $product->countArticle();
        $numberPages = ceil($totalArticle/$numberPerPage);
        $firstArticle = ($page * $numberPerPage) - $numberPerPage;
        $articles = $product->getArticlesAll($firstArticle, $numberPerPage);
        $categories = $product->getCategoriesBYArticles();
//        dd($categories);
        return $this->template->render('home.twig', ["articles"=> $articles,
                                                            "numberPages"=>$numberPages,
                                                            "i"=>$i,
                                                            "page"=>$page,
                                                            "categorie" => $categories
                                                            ]);
    }

    public function produit() : string
    {
        $product = new ProductModel();
        $id=$_GET['id'];
        $produit = $product->getArticleById($id);

        return $this->template->render('produit.twig',['produit'=>$produit]);
    }

    public function bagUser() : string
    {
        $product = new ProductModel();
        $userId = $_SESSION['id'];
        $productId=$product->getArticleBagId($userId);
//        dd($productId);

        return $this->template->render('users/panier.twig',["productId"=>$productId]);
    }
    public function paiementUser() : string
    {
        return $this->template->render('users/paiement.twig',);
    }
}
