<?php

namespace App\Controllers;
use App\Models\SearchProductModel;

class SearchProductController extends Controller
{
    public function search()
    {
        $searchProduct = new SearchProductModel();
        $terme = $_POST['search'];
        $articles = $searchProduct->search($terme);
        echo $this->template->render('home.twig', ["articles"=> $articles]);

    }
}