<?php

namespace App\Controllers;

use App\Models\BagModel;
use App\Models\ProductModel;

class BagUserController
{
    public function addBagUser(): void
    {
        $userModel = new BagModel();
        $userId = $_SESSION['id'];
        $articleId=$_POST['id'];
        $quantity= $_POST['quantity'];
        $quantityArticle = $_SESSION['quantityArticle'];

        $productBagQuantity = $userModel->countArticleById($userId,$articleId);
        if($productBagQuantity > 0){
            $newQuantity = $productBagQuantity['quantity'] + $quantity;
            $userModel->updateArticleById($newQuantity,$userId,$articleId);
        }else{
            $userModel->addBag($userId,$articleId,$quantity);
        }
            $this->changeQuantityByArticle($articleId,$quantity);
            $_SESSION['quantityArticle'] = (int)$userModel->countAllByUserId($userId);
//            dd($_SESSION['quantityArticle']);
            header('Location: /bag');

    }
    public function deleteArticleBag(): void
    {
        $userModel = new BagModel();
        $articleId = $_POST["id"];
        $userId = $_SESSION['id'];
        $quantityChoice=$_POST['quantity'];
//        $deleteArticle = $_SESSION['quantityArticle'];
//        $_SESSION['quantityArticle'] = $deleteArticle-1;
        $userModel->deleteArticleBagId($articleId,$userId);
        $this->addQuantityByArticle($articleId,$quantityChoice);
        $_SESSION['quantityArticle'] = (int)$userModel->countAllByUserId($userId);
        header('Location: /bag');
    }

    private function changeQuantityByArticle($articleId,$quantity): void
    {
        $product = new ProductModel();
        $articleQuantity = $product->getArticleById($articleId);
        $changeQuantity = $articleQuantity['quantity'] - $quantity;
        $product->updateArticleById($articleId, $changeQuantity);
    }

    private function addQuantityByArticle($articleId,$quantityChoice): void
    {
        $product = new ProductModel();
        $articleQuantity = $product->getArticleById($articleId);
        $changeQuantity = $articleQuantity['quantity'] + $quantityChoice;
        $product->updateArticleById($articleId, $changeQuantity);
    }
}
