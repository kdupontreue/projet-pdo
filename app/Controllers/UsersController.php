<?php

namespace App\Controllers;

use App\Models\UsersModel;
use App\Validators\UsersValidator;
use App\templating\Template;
use JetBrains\PhpStorm\NoReturn;

class UsersController
{
    public function CreateUser(): void
    {

        $userModel = new usersModel();
        $validator = new UsersValidator();

        if(isset($_POST['email']) && isset($_POST['password']) && isset($_POST['password2'])
            && !empty($_POST['email']) && !empty($_POST['password']) && !empty($_POST['password2']) )
        {
            $password = $_POST['password'];
            $password2 = $_POST['password2'];
            $email = $_POST['email'];

            if($password === $password2)
            {
                if($validator->UserValidate($email, $password))
                {
                    $password_hash = password_hash($password,PASSWORD_ARGON2I );
                    $userModel->CreateUserModel($password_hash, $email);
                    header('Location: /connexion');
                    exit;
                }
            }else{
                header('Location: /inscription');
            }
        }
    }

    public function Connect(): void
    {
        $userModel = new usersModel();
        $validator = new UsersValidator();

        if(isset($_POST['email']) && isset($_POST['password']) && !empty($_POST['email']) && !empty($_POST['password'])){
            $password = $_POST['password'];
            $email = $_POST['email'];
            $user = $userModel->ConnectUser($email);


        }

        $validator->UserValidate($email, $password);

            if(password_verify($password, $user['password']))
        {
            $_SESSION['id'] = $user['id'];
            header('Location: /home');
            $_SESSION['user'] = $email;
            exit;
        }else{

            header('Location: /connexion');
            exit;
        }

    }

    #[NoReturn] public function Disconnect(): void
    {
        session_unset();
        session_destroy();
        exit;
    }
}