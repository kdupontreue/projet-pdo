<?php

namespace App\Models;


class UsersModel extends Connection
{
    public function CreateUserModel($password_hash, $email)
    {
        $sql = 'INSERT INTO users(email, password) VALUES (:email, :password)';
        $query = $this->connection->prepare($sql);
        $query->bindValue(':email', $email, \PDO::PARAM_STR);
        $query->bindValue(':password', $password_hash, \PDO::PARAM_STR);
        $query->execute();
    }
    public function ConnectUser($email)
    {
        $sql = 'SELECT * FROM users WHERE email=:email';
        $query = $this->connection->prepare($sql);
        $query->bindValue(':email', $email, \PDO::PARAM_STR);
        $query->execute();
        $user = $query->fetch();
        return $user;
    }
}