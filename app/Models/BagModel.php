<?php

namespace App\Models;

class BagModel extends Connection
{
    public function countArticleById($userId,$articleId)
    {
        $sql = 'select quantity from bags where user_id = :userId and article_id = :articleId';
        $query = $this->connection->prepare($sql);
        $query->bindValue(':userId',$userId, \PDO::PARAM_INT);
        $query->bindValue(':articleId',$articleId, \PDO::PARAM_INT);
        $query->execute();
        return $query->fetch();
    }


    public function updateArticleById($newQuantity,$userId,$articleId)
    {
        $sql = "UPDATE bags set quantity = :newQuantity where user_id = :userId and article_id = :articleId";
        $query = $this->connection->prepare($sql);
        $query->bindValue(':newQuantity', $newQuantity, \PDO::PARAM_INT);
        $query->bindValue(':userId', $userId, \PDO::PARAM_INT);
        $query->bindValue(':articleId', $articleId, \PDO::PARAM_INT);
        $query->execute();

    }

    public function addBag($userId,$articleId,$quantity): void
    {
        $sql = 'INSERT INTO bags(user_id, article_id, quantity) values (:userId, :articleId, :quantity)';
        $query= $this->connection->prepare($sql);
        $query->bindValue(':userId', $userId, \PDO::PARAM_INT);
        $query->bindValue(':articleId', $articleId, \PDO::PARAM_INT);
        $query->bindValue(':quantity', $quantity, \PDO::PARAM_INT);
        $query->execute();
    }

    public function deleteArticleBagId($articleId,$userId): void
    {
        $sql = 'DELETE FROM bags where user_id = :userId and article_id = :articleId';
        $query= $this->connection->prepare($sql);
        $query->bindValue(':articleId', $articleId, \PDO::PARAM_INT);
        $query->bindValue(':userId', $userId, \PDO::PARAM_INT);
        $query->execute();
    }

    public function countAllByUserId($userId)
    {
        $sql = 'select Count(article_id) from Bags where user_id = :userId';
        $query = $this->connection->prepare($sql);
        $query->bindValue(':userId',$userId, \PDO::PARAM_INT);
        $query->execute();
        return $query->fetchColumn();
    }



}