<?php

namespace App\Models;

class ProductModel extends Connection
{

    public function getArticlesAll($firstarticle, $numberPerPage): bool|array
    {
        $sql = "SELECT * FROM articles LIMIT $firstarticle, $numberPerPage ";
        $query = $this->connection->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    public function getArticleById($id)
    {
        $sql = "SELECT * FROM articles WHERE id = $id";
        $query = $this->connection->prepare($sql);
        $query->execute();
        return $query->fetch();
    }

    public function countArticle()
    {
        $sql = "select COUNT(id) FROM articles";
        $query = $this->connection->prepare($sql);
        $query->execute();
        return $query->fetchColumn();
    }

    public function getArticleBagId($userId): bool|array
    {
        $sql = "SELECT id, name, brand, price, bags.quantity FROM articles LEFT JOIN bags on id = article_id where user_id = :userId";
        $query = $this->connection->prepare($sql);
        $query->bindValue(':userId', $userId, \PDO::PARAM_INT);
        $query->execute();
        return $query->fetchAll();
    }

    public function updateArticleById($articleId, $changeQuantity)
    {
        $sql = "UPDATE articles set quantity = :changeQuantity where id = :articleId";
        $query = $this->connection->prepare($sql);
        $query->bindValue(':articleId', $articleId, \PDO::PARAM_INT);
        $query->bindValue(':changeQuantity', $changeQuantity, \PDO::PARAM_INT);
        $query->execute();
    }
    public function getCategoriesBYArticles(): bool|array
    {
        $sql =' select distinct categories FROM articles';
        $query= $this->connection->prepare($sql);
        $query->execute();
        return $query->fetchAll();

    }
}

