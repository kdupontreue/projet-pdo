<?php

namespace App\Models;

class SearchProductModel extends Connection
{
    public function search($terme): bool|array
    {
        $sql = 'select * FROM articles where name like "%" :terme "%" or brand like "%" :terme "%" or description like "%" :terme "%" or categories like "%" :terme "%"  ';
        $query = $this->connection->prepare($sql);
        $query->bindValue(':terme', $terme, \PDO::PARAM_STR);
        $query->execute();
        return $query->fetchAll();
    }

}