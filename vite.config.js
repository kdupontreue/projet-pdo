import liveReload from 'vite-plugin-live-reload'
import {defineConfig} from 'vite';

export default defineConfig({
    plugins: [
        liveReload(['*/*/**/**/*.twig','*/*/**/*.twig','./*.twig', './*.php', '*/**/**/**/*.php', './*.js', './*/**/**/**/*.js'])
    ],
    build: {
        // generate manifest.json in outDir
        manifest: true,
        rollupOptions: {
            // overwrite default .html entry
            input: '/resources/assets/javascript/main.js'
        }
    }
})
