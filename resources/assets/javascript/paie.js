export default {
    view: async () => {
        return `
<div class="w-[850px] h-auto bg-yellow-100 flex flex-col justify-center mt-10 items-center">
    <h1 class="text-2xl mt-4">Nos differents moyens de livraison</h1>
    <div class="w-full h-20 text-xl flex mt-10 justify-around items-center">
            <span class="flex">
                <input type="radio" name="paiement" value="25.50">
                <p>25€50</p>
            </span>
        <p>Livraison Sur, livrée en moins d'une semaine</p>
    </div>
    <div class="w-full h-20 h-auto text-xl flex mt-4 justify-around items-center">
            <span class="flex">
                <input type="radio" name="paiement" value="54.50">
                <p>54€50</p>
            </span>
        <p>Livraison Sur, livrée en moins d'une semaine</p>
    </div>
    <div class="w-full h-20 h-auto text-xl flex mt-4 justify-around items-center">
            <span class="flex">
                <input type="radio" name="paiement" value="150.50">
                <p>150€50</p>
            </span>
        <p>Livraison Sur, livrée en moins d'une semaine</p>
    </div>
    <a href="../paiement#recapitulatif"
       class="mt-10 border flex items-center border-2 bg-black text-white w-auto px-4 h-10 mb-4 hover:text-black hover:bg-white"
       type="submit">valider Votre Choix de livraison</a>
</div>
            `
    },
    after: async () => {
        console.log("Component Home mounted");
    }
};