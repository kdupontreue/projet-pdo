import 'vite/modulepreload-polyfill'
import './navigation.js'
import ROUTER from "../../../router.js";

window.addEventListener('load', async () => {
    await ROUTER();
});


let arrowElement = document.getElementById('arrow')
let menuElement = document.getElementById('menu')
// let groupMenuElement = document.getElementById('groupMenu')
let categoriesElement = document.getElementById('categories')
let listElement = document.getElementById('list')
let ArrowlistElement = document.getElementById('arrowList')

arrowElement.addEventListener('click', function (){
    menuElement.classList.toggle('w-[175px]')
    categoriesElement.classList.toggle('hidden')
    listElement.classList.toggle('hidden')
    ArrowlistElement.classList.toggle('rotate-180')
})
