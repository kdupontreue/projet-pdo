<?php

namespace Db;

use App\Models\Connection;

final class Migrations extends Connection
{
    public function CreateDataBase()
    {
        $queries = [
            /** table principale */
            'CREATE TABLE if NOT EXISTS users(id int(11) PRIMARY KEY AUTO_INCREMENT, email varchar(320) NOT NULL, password varchar(255) NOT NULL)',

            'CREATE TABLE if NOT EXISTS articles(id int(11) PRIMARY KEY AUTO_INCREMENT, name varchar(200) NOT NULL, brand varchar(55) NOT NULL, img varchar(500) NOT NULL, description Text NOT NULL, price DECIMAL(20.2) NOT NULL, quantity INT(11) NOT NULL, categories Varchar(50) NOT NULL)',


            /** table pivot */
            'CREATE TABLE if NOT EXISTS bags(user_id int(11),article_id int(11), quantity int(11))',


            /** Liaison des tables */
            'ALTER TABLE bags ADD CONSTRAINT FK_bags_users FOREIGN KEY (user_id) REFERENCES users (id)',
            'ALTER TABLE bags ADD CONSTRAINT FK_bags_articles FOREIGN KEY (article_id) REFERENCES articles (id)',

        ];

        foreach ($queries as $query) {
            try {
                $statement = $this->connection->prepare($query);
                $statement->execute();
            } catch (Exception $e) {
                echo 'Erreur : ' . $e->getMessage() .'<br/>';
            }
        }

    }

}